include build/utils.mk

prepare-dir-%: 
	rm -f bin/lab${*}/lab${*};
	mkdir -p bin/lab${*};

build-%: 
	make prepare-dir-${*};
	@[ ! -z ${target} ] && \
		g++ -o bin/lab${*}/$(basename ${target}) sources/lab${*}/${target} -fopenmp || \
		g++ -o bin/lab${*}/lab${*} sources/lab${*}/main.cpp -fopenmp;

mbuild-%: 
	make prepare-dir-${*};
	@[ ! -z ${target} ] && \
		mpic++ -o bin/lab${*}/$(basename ${target}) sources/lab${*}/${target} \
		|| mpic++  -o bin/lab${*}/lab${*} sources/lab${*}/main.cpp \


run-%:
	@[ ! -z ${target} ] && \
		./bin/lab${*}/$(basename ${target}) ${args} || \
		./bin/lab${*}/lab${*} ${args};

mrun-%:
	@echo "n=${np}"
	@[ ! -z ${target} ] && \
		mpirun -np ${np}  ./bin/lab${*}/$(basename ${target}) ${args} || \
		mpirun -np ${np} ./bin/lab${*}/lab${*} ${args};

