#include <iostream>
#include <math.h>
#include <omp.h>

#define N 4

int main(int argc, char *argv[]) {
    double a[N]; a[0]=1; 
    double w;
    #pragma omp parallel for private(w) ordered
    for (int i=1; i<N; i++)
    {
        w = sin(i/double(N)*3.14); 
        #pragma omp ordered 
        {
            a[i] = w*a[i-1];
        }
    }
    for (int i=0; i<N; i++) {
        std::cout << (a[i]) << " ";
    }
    std::cout << std::endl;
    return 0;
}
