#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>

const int MAX_ITERATIONS = 100;
const double TOLERANCE = 1e-6;

int main(int argc, char* argv[]) {
    auto start_time = std::chrono::high_resolution_clock::now();
    const int N = std::atoi(argv[1]);

    std::vector<double> matrixA(N * N);
    std::vector<double> vectorB(N);

    auto nameString = std::string("assets/lab12/lab12-sources_") + std::to_string(N) + ".txt";
    std::cout << "using file: " << nameString << std::endl;
    std::ifstream inputFile(nameString);
    if (!inputFile.is_open()) {
        std::cerr << "Error while open file" << std::endl;
        return 1;
    }

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            inputFile >> matrixA[i * N + j];
        }
        inputFile >> vectorB[i];
    }

    inputFile.close();

    std::vector<double> localX(N, 0.0);

    for (int iter = 0; iter < MAX_ITERATIONS; iter++) {
        std::vector<double> localNewX(N, 0.0);

        for (int i = 0; i < N; i++) {
            double sum = 0;
            for (int j = 0; j < N; j++) {
                if (i != j) {
                    sum += matrixA[i * N + j] * localX[j];
                }
            }
            localNewX[i] = (vectorB[i] - sum) / matrixA[i * N + i];
        }

        std::vector<double> localDiff(N, 0.0);
        for (int i = 0; i < N; i++) {
            localDiff[i] = localNewX[i] - localX[i];
        }

        double maxDiffNumber = *std::max_element(localDiff.begin(), localDiff.end());

        if (maxDiffNumber < TOLERANCE) {
            std::cout << "tolerance: " << maxDiffNumber << std::endl;
            for (int i = 0; i < N; i++) {
                std::cout << "x[" << i << "]=" << localX[i] << std::endl;
            }
            break;
        }

        localX = localNewX;
    }

    auto end_time = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    std::cout << "Time: " << duration.count() << " mcs" << std::endl;
    return 0;
}
