#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <mpi.h>


const int MAX_ITERATIONS = 100;
const double TOLERANCE = 1e-6;

int main(int argc, char* argv[]) {
    auto start_time = std::chrono::high_resolution_clock::now();
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    const int N = size;

    std::vector<double> matrixA(N*N);
    std::vector<double> vectorB(N);

    if (rank == 0) {
        auto nameString = std::string("assets/lab12/lab12-sources_") + std::to_string(size) + ".txt";
        std::cout << "using file: " << nameString << std::endl;
        std::ifstream inputFile(nameString);
        if (!inputFile.is_open()) {
            std::cerr << "Error while open file" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
            return 1;
        }

        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                inputFile >> matrixA[i * N + j];
            }
            inputFile >> vectorB[i];
        }

        inputFile.close();
    }

    std::vector<double> localA(N);
    MPI_Scatter(matrixA.data(), N, MPI_DOUBLE, localA.data(), N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    double localB = 0;
    MPI_Scatter(vectorB.data(), 1, MPI_DOUBLE, &localB, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    std::vector<double> localX(N, 0.0);

    for (int iter = 0; iter < MAX_ITERATIONS; iter++) {
        std::vector<double> localNewX(N, 0.0);

        double sum = 0;
        for (int i = 0; i < N; i++) {
            if (i != rank) {
                sum += localA[i] * localX[i];
            }
        }
        localNewX[rank] = (localB - sum) / localA[rank];
        
        std::vector<double> localDiff(N, 0.0);
        localDiff[rank] = localNewX[rank] - localX[rank];
        
        std::vector<double> maxDiff(N, 0.0);
        MPI_Allreduce(localDiff.data(), maxDiff.data(), N, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        MPI_Allreduce(localNewX.data(), localX.data(), N, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        double maxDiffNumber = *std::max_element(maxDiff.begin(), maxDiff.end());
        if (maxDiffNumber < TOLERANCE) {
            if (rank == 0) {
                std::cout << "tolerance: " << maxDiffNumber << std::endl;
            }
            std::cout << "x[" << rank << "]=" << localX[rank] << std::endl;
            break;
        }
    }
    
    if (rank == 0) {
        auto end_time = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
        std::cout << "Time: " << duration.count() << " mcs" << std::endl;
    } 

    MPI_Finalize();
    return 0;
}
