#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define MAX_GROUPS 100
#define MAX_STUDENTS_PER_GROUP 50

float generateRandomGrade() {
    return (float)(rand() % 101) / 10.0;
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <num_threads> <num_groups>\n", argv[0]);
        return 1;
    }

    int numThreads = atoi(argv[1]);
    int numGroups = atoi(argv[2]);

    if (numThreads > numGroups) {
        printf("Number of threads should be less than or equal to the number of groups.\n");
        return 1;
    }

    float grades[MAX_GROUPS][MAX_STUDENTS_PER_GROUP];

    for (int i = 0; i < numGroups; ++i) {
        for (int j = 0; j < MAX_STUDENTS_PER_GROUP; ++j) {
            grades[i][j] = generateRandomGrade();
        }
    }

    float totalGrade = 0.0;
    int totalStudents = numGroups * MAX_STUDENTS_PER_GROUP;

    #pragma omp parallel for num_threads(numThreads) reduction(+:totalGrade)
    for (int i = 0; i < numGroups; ++i) {
        for (int j = 0; j < MAX_STUDENTS_PER_GROUP; ++j) {
            totalGrade += grades[i][j];
        }
    }

    float courseAverage = totalGrade / totalStudents;

    printf("Course Average Grade: %.2f\n", courseAverage);

    #pragma omp parallel for num_threads(numThreads)
    for (int i = 0; i < numGroups; ++i) {
        float groupTotal = 0.0;

        for (int j = 0; j < MAX_STUDENTS_PER_GROUP; ++j) {
            groupTotal += grades[i][j];
        }

        float groupAverage = groupTotal / MAX_STUDENTS_PER_GROUP;
        printf("Group %d Average Grade: %.2f\n", i + 1, groupAverage);
    }

    return 0;
}
