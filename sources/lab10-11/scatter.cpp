#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
    int rank, size, i, n;
    double start_time = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    if (rank == 0) {
        start_time = MPI_Wtime(); 
    }

    int sendbuf[size];
    int recvbuf;

    for (int i=0; i<size; i++)
        sendbuf[i] = 1 + rank + size*i;

    std::cout << "Proc inp" <<  rank << ": ";
    for (int i = 0; i < size; i++) std::cout << sendbuf[i] << " ";
    std::cout << std::endl;

    int recvcounts[size];
    for (int i=0; i<size; i++)
        recvcounts[i] = 1;

    MPI_Reduce_scatter(sendbuf, &recvbuf, recvcounts, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    std::cout << "Proc res " << rank << " " << recvbuf << std::endl;

    if (rank == 0) {
        double end_time = MPI_Wtime();
        std::cout << "Proc res " << rank << ": " << recvbuf << std::endl;
        std::cout << "Time taken by process " << rank << ": seconds " << std::fixed << end_time - start_time << std::endl;
    }

    MPI_Finalize();

    return 0;
}