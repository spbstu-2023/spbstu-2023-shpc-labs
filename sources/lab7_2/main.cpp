#include <iostream>
#include <vector>
#include <math.h>
#include <omp.h>
#include <stdlib.h>

struct Vector {
    double x, y, z;

    Vector(double x, double y, double z) : x(x), y(y), z(z) {}
};

bool areVectorsCoplanar(const Vector& v1, const Vector& v2, const Vector& v3) {
    double crossProductX = (v2.y - v1.y) * (v3.z - v1.z) - (v2.z - v1.z) * (v3.y - v1.y);
    double crossProductY = (v2.z - v1.z) * (v3.x - v1.x) - (v2.x - v1.x) * (v3.z - v1.z);
    double crossProductZ = (v2.x - v1.x) * (v3.y - v1.y) - (v2.y - v1.y) * (v3.x - v1.x);

    double magnitude = sqrt(crossProductX * crossProductX + crossProductY * crossProductY + crossProductZ * crossProductZ);

    double epsilon = 1e-10;

    return std::abs(magnitude) < epsilon;
}

std::vector<Vector> vectors;

int N;

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cerr << "N not defile" << std::endl;
        return 1;
    }

    N = atoi(argv[1]);
    
    if (N < 3) {
        std::cerr << "N must be more that 3" << std::endl;
        return 1;
    }    

    for (int i = 0; i < N; i++) {
        double x, y, z;
        std::cin >> x >> y >> z;
        vectors.push_back(Vector(x, y, z));
    }

    int numVectors = static_cast<int>(vectors.size());

    #pragma omp parallel for
    for (int i = 0; i < numVectors - 2; ++i) {
        for (int j = i + 1; j < numVectors - 1; ++j) {
            for (int k = j + 1; k < numVectors; ++k) {
                if (areVectorsCoplanar(vectors[i], vectors[j], vectors[k])) {
                    #pragma omp critical
                    {
                        std::cout << "Coplanar Vectors: (" << vectors[i].x << ", " << vectors[i].y << ", " << vectors[i].z << "), "
                                  << "(" << vectors[j].x << ", " << vectors[j].y << ", " << vectors[j].z << "), "
                                  << "(" << vectors[k].x << ", " << vectors[k].y << ", " << vectors[k].z << ")\n";
                    }
                }
            }
        }
    }

    return 0;
}
