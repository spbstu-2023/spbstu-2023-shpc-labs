## About
**Цели**: Разработка и отладка OpenMP-приложения.

**Постановка задачи**: Найти все возможные тройки компланарных векторов. Входные данные: множество не равных между собой векторов `(x, y, z)`, где `x`, `y`, `z` – числа. Количество потоков зависит от мощности множества векторов, и не является параметром задачи.


## Examples
Компланарные вектора - это вектора, *детерминанг* которых близок к нулю (для C++ будем считать это число равным `1e-10`).

Примеры компланарных векторов:
$$ 
\begin{matrix}
a = (1, 2, 3) \\
b = (5, 2, 5) \\
c = (7, 8, 9)
\end{matrix}
$$

Объяснение:
$$
\bar{a} * (\bar{b} * \bar{c}) = 
\begin{matrix}
 a_{x} a_{y} a_{z} \\ 
 b_{x} b_{y} b_{z} \\ 
 c_{x} c_{y} c_{z} 
\end{matrix} = 
\begin{matrix}
 1 2 3 \\ 
 4 5 6 \\ 
 7 8 9 
\end{matrix} = 1 * 5 * 9 + 2 * 6 * 7 + 3 * 4 * 8 - 3 * 5 * 7 - 2 * 4 * 9 - 1 * 6 * 8 = 45 + 84 + 96 - 105 - 72 - 48 = 0
$$


Пример некомпланарных векторов:
$$ 
\begin{matrix}
a = (1, 2, 3) \\
b = (5, 2, 5) \\
c = (7, 8, 9)
\end{matrix}
$$

Объяснение:
$$
\bar{a} * (\bar{b} * \bar{c}) = 
\begin{matrix}
 a_{x} a_{y} a_{z} \\ 
 b_{x} b_{y} b_{z} \\ 
 c_{x} c_{y} c_{z} 
\end{matrix} = 
\begin{matrix}
 1 2 3 \\ 
 5 2 5 \\ 
 7 8 9 
\end{matrix} = 1 * 2 * 9 + 2 * 5 * 7 + 3 * 5 * 8 - 3 * 2 * 7 - 2 * 5 * 9 - 1 * 5 * 8 = 18 + 70 + 120 - 42 - 90 - 40 = 36
$$

## Build
Для сборки лабораторной работы выполнить:
```bash
    make docker-build-7_2
```

## Run
Для запуска лабораторной работы выполнить:
```bash
    make docker-run-7_2 args="5"
```
