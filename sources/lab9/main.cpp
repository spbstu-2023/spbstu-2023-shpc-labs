#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <mpi.h>

std::string readFileData(char* filename) {
    MPI_File file;
    MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &file);

    MPI_Offset filesize;
    MPI_File_get_size(file, &filesize);
    
    std::string data;
    data.resize(filesize);
    MPI_File_read_at_all(file, 0, const_cast<char*>(data.c_str()), filesize, MPI_CHAR, MPI_STATUS_IGNORE);
    MPI_File_close(&file);

    return data;
}

std::vector<int> convertToIntVector(std::string input, int start, int end) {
    std::istringstream iss(input);
    std::vector<int> data;
    int value;
    while (iss >> value) {
        data.push_back(value);
    }
    return data;
}

std::vector<int> getChunk(std::vector<int> data, int start, int end) {
    std::vector<int> chunk;
    chunk.assign(data.begin() + start, data.begin() + end);

    return chunk;
}

long long calculate(std::vector<int> data) {
    long long result = 1;
     for (const auto& value : data) {
        result *= value;
    }
    return result;
}

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    std::string fileStringData = readFileData(argv[1]);
    std::vector<int> data = convertToIntVector(fileStringData, rank, rank + 5);

    int chunkSize = (data.size() / size);
    int start = rank * chunkSize;
    int end = (rank + 1) * chunkSize;

    if (rank == size - 1) {
        end += (data.size() % size);
    }

    std::vector<int> chunk = getChunk(data, start, end);

    std::cout << "Proc " << rank << ": ";
    for (const auto& value : chunk) {
        std::cout << value << " ";
    }
    std::cout << std::endl;

    long long local_result = calculate(chunk);
    std::cout << "Proc " << rank << " local_result: " << local_result << std::endl;

    std::vector<int> allResults(size);
    MPI_Gather(&local_result, 1, MPI_INT, allResults.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        long long result = calculate(allResults);
        std::cout << "Proc " << rank << " result: " << result << std::endl;   
    }
    MPI_Finalize();

    return 0;
}
