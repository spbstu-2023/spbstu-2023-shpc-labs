# СРЕДСТВА РАЗРАБОТКИ ПАРАЛЛЕЛЬНЫХ ПРОГРАММ

## About
Name: Лабораторные работы по дициплине Средства разработки параллельных программ

Authors: Andrianov Artemiy

Group: #5140904/30202

Variant: 11

## Prerequisite
- `g++` & `gcc`
- `openmpi-bin` & `libopenmpi-dev`
- Optional: Docker 

## References
- [x] [Лабораторная работа #6](sources/lab6/)
- [x] [Лабораторная работа #7.1](sources/lab7_1/)
- [x] [Лабораторная работа #7.2](sources/lab7_2/)
- [x] [Лабораторная работа #9](sources/lab9/)
- [x] [Лабораторная работа #10-11](sources/lab10-11/)
- [x] [Лабораторная работа #12](sources/lab12/)

## Build labs
Для сборки лабораторной работы 
- На host-машине
```bash
    make build-<lab-number> target="name-of-file" args="build-args" 
```

- В настроенном docker окружении
```bash
    make docker-build-<lab-number> target="name-of-file" args="build-args"
```

> Для сборки работ с использованием MPI - используйте вариант таргета `mbuild-%` и его Docker аналог `docker-mbuild-%`.

## Run labs
Для запуска лабораторной работы 
- На host-машине выполнить:
```bash
    make run-<lab-number> target="name-of-file" args="run-args"
```

- В настроенном docker окружении выполнить:
```bash
    make docker-run-<lab-number> target="name-of-file" args="run-args"
```

> Для запуска работ с использованием MPI - используйте вариант таргета `mrun-%` и его Docker аналог `docker-run-%`.
